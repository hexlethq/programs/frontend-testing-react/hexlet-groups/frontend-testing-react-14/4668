const faker = require('faker');

const port = 5000;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

describe('simple blog works', () => {
  it('should open main page', async () => {
    await page.goto(appUrl);

    await expect(page).toMatch('Welcome to a Simple blog!');
  });

  it('should return list of articles', async () => {
    await page.goto(appArticlesUrl);

    const articleList = await page.$$('table tbody tr');

    expect(articleList.length).toBeGreaterThan(0);
  });

  it('should open new article form', async () => {
    await page.goto(appArticlesUrl);
    await Promise.all([page.waitForNavigation(), page.click('[data-testid="article-create-link"]')]);

    await expect(page).toMatch('Create article');
    await expect(page).toMatchElement('form');
  });

  it('should create new article', async () => {
    const name = faker.name.title();
    const content = faker.lorem.paragraph();

    await page.goto(`${appArticlesUrl}/new`);

    await expect(page).toFillForm('form', {
      'article[name]': name,
      'article[content]': content,
    });
    await expect(page).toSelect('[name="article[categoryId]"]', '1');

    await Promise.all([page.waitForSelector('table'), page.click('[data-testid="article-create-button"]')]);

    await expect(page).toMatch(name);
  });

  it('should edit exist article', async () => {
    const name = faker.name.title();
    const content = faker.lorem.paragraph();

    await page.goto(`${appArticlesUrl}/4/edit`);

    await expect(page).toFillForm('form', {
      'article[name]': name,
      'article[content]': content,
    });
    await expect(page).toSelect('[name="article[categoryId]"]', '1');
    await expect(page).toClick('[data-testid="article-update-button"]');

    await page.waitForSelector('table');

    await expect(page).toMatch(name);
  });
});
