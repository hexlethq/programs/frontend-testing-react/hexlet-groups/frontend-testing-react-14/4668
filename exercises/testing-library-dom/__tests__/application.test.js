// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const faker = require('faker');
const testingLibraryDom = require('@testing-library/dom');
const testingLibraryUserEvent = require('@testing-library/user-event');

const run = require('../src/application');

const { screen } = testingLibraryDom;
const userEvent = testingLibraryUserEvent.default;

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
const createTask = (text) => {
  userEvent.type(screen.getByRole('textbox', { name: /new task name/i }), text);
  userEvent.click(screen.getByRole('button', { name: /add task/i }));
};

describe('app', () => {
  it('should render empty list by default', () => {
    expect(screen.getByText(/general/i)).toBeVisible();
    expect(document.querySelector('[data-container="tasks"]')).toBeEmptyDOMElement();
  });

  it('should add tasks to default list and reset task input', () => {
    const task1 = faker.lorem.words();
    const task2 = faker.lorem.words();

    createTask(task1);
    createTask(task2);

    expect(screen.getByRole('textbox', { name: /new task name/i })).toHaveValue('');
    expect(screen.getByText(task1)).toBeVisible();
    expect(screen.getByText(task2)).toBeVisible();
  });

  it('should be able to add the second list and add new tasks', () => {
    const secondaryList = faker.lorem.word();
    const task1 = faker.lorem.words();
    const task2 = faker.lorem.words();

    createTask(task1);

    expect(screen.getByText(task1)).toBeVisible();

    userEvent.type(screen.getByRole('textbox', { name: /new list name/i }), secondaryList);
    userEvent.click(screen.getByRole('button', { name: /add list/i }));
    userEvent.click(screen.getByText(secondaryList));

    createTask(task2);

    expect(screen.getByText(secondaryList)).toBeVisible();
    expect(screen.getByText(task2)).toBeVisible();
  });
});
// END
