const puppeteer = require('puppeteer');
const faker = require('faker');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  // BEGIN
  it('should open main page', async () => {
    await page.goto(appUrl);
    const title = await page.$('#title');
    const result = await page.evaluate((el) => el.innerText, title);

    expect(result).toContain('Welcome to a Simple blog!');
  });

  it('should return list of articles', async () => {
    await page.goto(appArticlesUrl);
    const articles = await page.$$('#articles tr');

    expect(articles.length).toBeGreaterThan(0);
  });

  it('should open new article form', async () => {
    await page.goto(appArticlesUrl);
    await page.click('body > div > a');
    const result = await page.waitForSelector('form');

    expect(result).toBeTruthy();
  });

  it('should create new article', async () => {
    const name = faker.name.title();
    const content = faker.lorem.paragraph();

    await page.goto(`${appArticlesUrl}/new`);
    await page.type('#name', name);
    await page.select('#category', '1');
    await page.type('#content', content);
    await page.click('input[type=submit]');
    await page.waitForSelector('#articles');
    const result = await page.$eval('tbody > tr:last-child > td:nth-child(2)', (el) => el.innerText);

    expect(result).toBe(name);
  });

  it('should edit exist article', async () => {
    const name = faker.name.title();

    await page.goto(appArticlesUrl);
    await page.click('tbody > tr:nth-child(1) > td:nth-child(4) > a');
    await page.waitForSelector('#edit-form');
    const input = await page.$('#name');
    await input.click({ clickCount: 4 });
    await input.type(name);
    await page.click('input[type=submit]');
    await page.waitForSelector('#articles');
    const result = await page.$eval('tbody > tr:nth-child(1) > td:nth-child(2)', (el) => el.innerText);

    expect(result).toBe(name);
  });
  // END

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
