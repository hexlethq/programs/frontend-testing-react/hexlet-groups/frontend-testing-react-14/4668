// @ts-check

import React from 'react';
import nock from 'nock';
import axios from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';

import TodoBox from '../src/TodoBox.jsx';

axios.defaults.adapter = httpAdapter;
const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

afterAll(() => {
  nock.enableNetConnect();
});

// BEGIN
describe('todo app', () => {
  test('should render app', () => {
    nock(host).get('/tasks').reply(200, []);

    render(<TodoBox />);

    expect(screen.getByRole('textbox')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /add/i })).toBeInTheDocument();
  });

  test('should add tasks', async () => {
    nock(host).get('/tasks').reply(200, []);
    nock(host).post('/tasks', { text: 'task1' }).reply(200, { id: 1, text: 'task1', state: 'active' });
    nock(host).post('/tasks', { text: 'task2' }).reply(200, { id: 2, text: 'task2', state: 'active' });

    render(<TodoBox />);

    const input = screen.getByRole('textbox');
    const submit = screen.getByRole('button', { name: /add/i });

    userEvent.type(input, 'task1');
    userEvent.click(submit);

    expect(await screen.findByText('task1')).toBeInTheDocument();

    userEvent.type(input, 'task2');
    userEvent.click(submit);

    expect(await screen.findByText('task2')).toBeInTheDocument();
  });

  test('should toggle task state', async () => {
    const task = { id: 1, text: 'task1', state: 'active' };

    nock(host).get('/tasks').reply(200, [task]);
    nock(host)
      .patch('/tasks/1/finish')
      .reply(200, { ...task, state: 'finished' });
    nock(host)
      .patch('/tasks/1/activate')
      .reply(200, { ...task, state: 'active' });

    const { container } = render(<TodoBox />);

    userEvent.click(await screen.findByText(task.text));

    await waitFor(() => {
      expect(container.querySelector('.todo-finished-tasks')).toBeInTheDocument();
      expect(container.querySelector('.todo-active-tasks')).not.toBeInTheDocument();
    });

    userEvent.click(await screen.findByText(task.text));

    await waitFor(() => {
      expect(container.querySelector('.todo-active-tasks')).toBeInTheDocument();
      expect(container.querySelector('.todo-finished-tasks')).not.toBeInTheDocument();
    });
  });
});
// END
