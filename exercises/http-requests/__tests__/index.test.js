const nock = require('nock');
const axios = require('axios');

axios.defaults.adapter = require('axios/lib/adapters/http');

const { get, post } = require('../src/index.js');

const users = [
  {
    firstname: 'Ivan',
    lastname: 'Ivanov',
    age: 20,
  },
  {
    firstname: 'Igor',
    lastname: 'Frolov',
    age: 30,
  },
];

const url = new URL('https://example.com/users');

beforeAll(() => nock.disableNetConnect());
afterEach(() => nock.cleanAll());
afterAll(() => nock.enableNetConnect());

describe('users get', () => {
  it('should return users', async () => {
    const scope = nock(url.origin).get(url.pathname).reply(200, users);
    const result = await get(url.href);

    expect(result).toEqual(users);
    expect(scope.isDone()).toBeTruthy();
  });

  it('should throw error', async () => {
    const scope = nock(url.origin).get(url.pathname).reply(500);

    await expect(() => get(url.href)).rejects.toThrow(Error);
    expect(scope.isDone()).toBeTruthy();
  });
});

describe('users post', () => {
  const data = {
    firstname: 'Fedor',
    lastname: 'Sumkin',
    age: 33,
  };

  it('should return user', async () => {
    const scope = nock(url.origin).post(url.pathname, data).reply(200, data);
    const result = await post(url.href, data);

    expect(result).toMatchObject(data);
    expect(scope.isDone()).toBeTruthy();
  });

  it('should throw error', async () => {
    const scope = nock(url.origin).post(url.pathname, data).reply(500);

    await expect(() => post(url.href, data)).rejects.toThrow(Error);
    expect(scope.isDone()).toBeTruthy();
  });
});
