const axios = require('axios');

const get = async (url) => {
  try {
    const { data } = await axios.get(url);

    return data;
  } catch (error) {
    throw new Error(error);
  }
};

const post = async (url, data) => {
  try {
    const response = await axios.post(url, data);

    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = { get, post };
