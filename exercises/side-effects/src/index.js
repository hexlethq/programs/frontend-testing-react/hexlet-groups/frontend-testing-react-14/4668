const fs = require('fs');

const increment = (version, release = 'patch') => {
  let [major, minor, patch] = version.split('.').map(Number);

  switch (release) {
    case 'major':
      major += 1;
      minor = 0;
      patch = 0;
      break;
    case 'minor':
      minor += 1;
      patch = 0;
      break;
    case 'patch':
      patch += 1;
      break;
    default:
      throw new Error(`invalid increment argument: ${release}`);
  }

  return `${major}.${minor}.${patch}`;
};

const upVersion = (path, release) => {
  try {
    const data = fs.readFileSync(path, 'utf8');
    const parsedData = JSON.parse(data);
    const version = increment(parsedData?.version, release);

    fs.writeFileSync(path, JSON.stringify({ ...parsedData, version }));
  } catch (error) {
    throw new TypeError(error);
  }
};

module.exports = { upVersion };
