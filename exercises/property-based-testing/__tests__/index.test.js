const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

describe('array sort function', () => {
  it('should produce ordered array', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (array) => {
        expect(sort(array)).toBeSorted();
      }),
    );
  });

  it('should be sorted in descending order', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (array) => {
        expect(sort(array).reverse()).toBeSorted({ descending: true });
      }),
    );
  });

  it('should be idempotent', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (array) => {
        expect(sort(array)).toEqual(sort(sort(array)));
      }),
    );
  });

  it('should have initial items count', () => {
    fc.assert(
      fc.property(fc.array(fc.integer()), (array) => {
        expect(sort(array)).toHaveLength(array.length);
      }),
    );
  });
});
