// @ts-check

import '@testing-library/jest-dom';

import nock from 'nock';
import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Autocomplete from '../src/Autocomplete.jsx';

const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

// BEGIN
describe('autocomplete', () => {
  test('should render country list', async () => {
    nock(host).get('/countries').query({ term: 'a' }).reply(200, ['Afghanistan', 'Albania', 'Algeria']);
    nock(host).get('/countries').query({ term: 'al' }).reply(200, ['Albania', 'Algeria']);
    nock(host).get('/countries').query({ term: 'alg' }).reply(200, ['Algeria']);

    const { container, getByRole, queryByText } = render(<Autocomplete />);

    const input = getByRole('textbox');

    userEvent.type(input, 'a');

    await waitFor(() => {
      expect(queryByText(/afghanistan/i)).toBeVisible();
      expect(queryByText(/albania/i)).toBeVisible();
      expect(queryByText(/algeria/i)).toBeVisible();
    });

    userEvent.type(input, 'l');

    await waitFor(() => {
      expect(queryByText(/afghanistan/i)).toBeNull();
      expect(queryByText(/albania/i)).toBeVisible();
      expect(queryByText(/algeria/i)).toBeVisible();
    });

    userEvent.type(input, 'g');

    await waitFor(() => {
      expect(queryByText(/afghanistan/i)).toBeNull();
      expect(queryByText(/albania/i)).toBeNull();
      expect(queryByText(/algeria/i)).toBeVisible();
    });

    userEvent.clear(input);

    expect(container.querySelector('ul')).not.toBeInTheDocument();
  });
});
// END
