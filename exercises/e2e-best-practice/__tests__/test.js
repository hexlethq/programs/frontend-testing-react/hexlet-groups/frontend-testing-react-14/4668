// @ts-check
// BEGIN
const appUrl = 'http://localhost:8080';

const task1 = 'task1';
const task2 = 'task2';

describe('app', () => {
  beforeAll(async () => {
    await page.goto(appUrl);
  });

  it('should open app', async () => {
    await expect(page).toMatchElement('[data-testid="task-name-input"]');
    await expect(page).toMatchElement('[data-testid="add-task-button"]');
  });

  it('should create tasks', async () => {
    await expect(page).toFillForm('form', { text: task1 });
    await page.click('[data-testid=add-task-button]');
    await expect(page).toFillForm('form', { text: task2 });
    await page.click('[data-testid=add-task-button]');

    await expect(page).toMatch(task1);
    await expect(page).toMatch(task2);
  });

  it('should delete task', async () => {
    await page.click('[data-testid=remove-task-1]');

    await expect(page).not.toMatch(task1);
    await expect(page).toMatch(task2);
  });
});
// END
