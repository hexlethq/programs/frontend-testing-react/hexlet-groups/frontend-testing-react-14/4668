describe('Object.assign', () => {
  it('main', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src);

    expect(result).toEqual({ k: 'v', a: 'a', b: 'b' });
  });

  it('mutate target object', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src);

    expect(result).toBe(target);
  });

  it('clone object', () => {
    const src = { k: 'v', b: 'b' };
    const target = {};
    const result = Object.assign(target, src);

    expect(result).toEqual(src);
  });

  it('merge objects', () => {
    const o1 = { a: 1 };
    const o2 = { b: 2 };
    const o3 = { c: 3 };
    const result = Object.assign(o1, o2, o3);

    expect(result).toEqual({ a: 1, b: 2, c: 3 });
  });

  it('merge objects with same properties', () => {
    const o1 = { a: 1, b: 1, c: 1 };
    const o2 = { b: 2, c: 2 };
    const o3 = { c: 3 };
    const result = Object.assign(o1, o2, o3);

    expect(result).toEqual({ a: 1, b: 2, c: 3 });
  });

  it('deep clone object', () => {
    const target = { a: 0, b: { c: 0 } };
    const src = { b: { c: 1, d: 5 }, d: 0 };
    const result = Object.assign(target, src);

    expect(result).toEqual({ a: 0, b: { c: 1, d: 5 }, d: 0 });
  });

  it('clone primitives', () => {
    const v1 = 'abc';
    const v2 = true;
    const v3 = 10;
    const v4 = Symbol('foo');
    const target = {};
    const result = Object.assign(target, v1, v2, v3, v4);

    expect(result).toEqual({ 0: 'a', 1: 'b', 2: 'c' });
  });

  it('should throw exception when assign read-only property', () => {
    const target = Object.defineProperty({}, 'foo', {
      value: 1,
      writable: false,
    });
    const src = {
      bar: 2,
      foo2: 3,
      foo: 3,
      foo3: 3,
      baz: 4,
    };
    const result = () => Object.assign(target, src);

    expect(result).toThrow();
  });

  it('mutate target even throw exception when assign read-only property', () => {
    const target = Object.defineProperty({}, 'foo', {
      value: 1,
      writable: false,
    });
    const src = {
      bar: 2,
      foo2: 3,
      foo: 3,
      foo3: 3,
      baz: 4,
    };
    const result = () => Object.assign(target, src);

    expect(result).toThrow();
    expect(target.bar).toBe(2);
    expect(target.foo2).toBe(3);
    expect(target.foo).toBe(1);
    expect(target.foo3).toBeUndefined();
    expect(target.baz).toBeUndefined();
  });
});
