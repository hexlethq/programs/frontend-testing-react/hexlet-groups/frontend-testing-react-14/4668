// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const faker = require('faker');

const run = require('../src/application');

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
const defaultListId = 'general';

const getListsContainer = () => document.querySelector('[data-container="lists"]');
const getTasksContainer = () => document.querySelector('[data-container="tasks"]');

const getListInput = () => document.querySelector('[data-testid="add-list-input"]');
const getListButton = () => document.querySelector('[data-testid="add-list-button"]');

const getListLinkById = (id) => document.querySelector(`[data-testid="${id}-list-item"]`);

const getTaskInput = () => document.querySelector('[data-testid="add-task-input"]');
const getTaskButton = () => document.querySelector('[data-testid="add-task-button"]');

const createList = (newList) => {
  getListInput().value = newList;
  getListButton().click();
};
const createTask = (newTask) => {
  getTaskInput().value = newTask;
  getTaskButton().click();
};

it('Should render empty list by default', () => {
  const listsContainer = getListsContainer();

  expect(listsContainer).toHaveTextContent(/General/);
  expect(getTasksContainer()).toBeEmptyDOMElement();
});

it('Should add tasks to default list and reset task input', () => {
  const task1 = faker.lorem.words();
  const task2 = faker.lorem.words();

  createTask(task1);
  createTask(task2);

  expect(getTaskInput().value).toBe('');
  expect(getTasksContainer()).toHaveTextContent(task1);
  expect(getTasksContainer()).toHaveTextContent(task2);
});

it('Should be able to add the second list and add new tasks', () => {
  const secondaryList = faker.lorem.word();

  const defaultTaskList = [faker.lorem.words(), faker.lorem.words()];
  const secondaryTaksList = [faker.lorem.words(), faker.lorem.words()];

  defaultTaskList.forEach(createTask);

  createList(secondaryList);

  getListLinkById(secondaryList).click();

  secondaryTaksList.forEach(createTask);

  expect(getListInput().value).toBe('');
  expect(getListsContainer()).toHaveTextContent(secondaryList);

  secondaryTaksList.forEach((task) => {
    expect(getTasksContainer()).toHaveTextContent(task);
  });

  getListLinkById(defaultListId).click();

  defaultTaskList.forEach((task) => {
    expect(getTasksContainer()).toHaveTextContent(task);
  });
});
// END
