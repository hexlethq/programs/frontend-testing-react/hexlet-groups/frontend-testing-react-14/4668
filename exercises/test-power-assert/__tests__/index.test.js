const assert = require('power-assert');
const { flattenDepth } = require('lodash');

const array = [1, [2, [3, [4]], 5]];
const flattenArray = [1, 2, 3, 4, 5];

assert.deepEqual(flattenDepth([]), [], 'empty array');
assert.deepEqual(flattenDepth(array, -1), array, '-1 depth');
assert.deepEqual(flattenDepth(array, 0), array, '0 depth');
assert.deepEqual(flattenDepth(array), [1, 2, [3, [4]], 5], '1 depth');
assert.deepEqual(flattenDepth(array, 2), [1, 2, 3, [4], 5], '2 depth');
assert.deepEqual(flattenDepth(array, 3), flattenArray, '3 depth');
assert.deepEqual(flattenDepth(array, 10), flattenArray, '10 depth');
