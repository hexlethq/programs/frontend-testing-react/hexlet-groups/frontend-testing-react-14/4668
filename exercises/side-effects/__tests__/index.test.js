const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

const getFixturePath = (filePath) => path.resolve(__dirname, `../__fixtures__/${filePath}`);
const parseFile = (filePath) => JSON.parse(fs.readFileSync(filePath, 'utf8'));

const packagePath = getFixturePath('package.json');

describe('upVersion', () => {
  afterEach(() => {
    fs.writeFileSync(packagePath, '{"version":"1.3.2"}');
  });

  test.each([
    ['patch', '1.3.3'],
    ['minor', '1.4.0'],
    ['major', '2.0.0'],
  ])('should update %s', (release, expected) => {
    upVersion(packagePath, release);

    const { version } = parseFile(packagePath);

    expect(version).toBe(expected);
  });

  it('should update patch version by default', () => {
    upVersion(packagePath);

    const { version } = parseFile(packagePath);

    expect(version).toBe('1.3.3');
  });

  it('should return error for wrong release name', () => {
    expect(() => upVersion(packagePath, 'prerelease')).toThrow(TypeError);
  });

  it('should return error for wrong file path', () => {
    expect(() => upVersion()).toThrow(TypeError);
  });
});
