const faker = require('faker');

describe('faker helpers.createTransaction', () => {
  it('should return transaction', () => {
    const transaction = faker.helpers.createTransaction();

    expect(transaction).toMatchObject({
      amount: expect.stringMatching(/\d+/g),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.stringMatching(/\d+/g),
    });
  });

  it('should return uniq transaction', () => {
    const transactionA = faker.helpers.createTransaction();
    const transactionB = faker.helpers.createTransaction();

    expect(transactionA).not.toMatchObject(transactionB);
    expect(transactionA).not.toBe(transactionB);
  });
});
